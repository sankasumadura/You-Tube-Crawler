

#you should generate and include client secret data into CLIENT.json

VIDEO_ID = '61TAqY03xwk'

import httplib2
import os
import sys
import argparse 
from googleapiclient.discovery import build_from_document
from googleapiclient.errors import HttpError
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import argparser, run_flow



CLIENT_SECRETS_FILE = "CLIENT.json"

# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account and requires requests to use an SSL connection.
YOUTUBE_READ_WRITE_SSL_SCOPE = "https://www.googleapis.com/auth/youtube.force-ssl"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

# This variable defines a message to display if the CLIENT_SECRETS_FILE is
# missing.
MISSING_CLIENT_SECRETS_MESSAGE = """
WARNING: Please configure OAuth 2.0

To make this sample run you will need to populate the client_secrets.json file
found at:
   %s
with information from the APIs Console
https://console.developers.google.com

For more information about the client_secrets.json file format, please visit:
https://developers.google.com/api-client-library/python/guide/aaa_client_secrets
""" % os.path.abspath(os.path.join(os.path.dirname(__file__),
                                   CLIENT_SECRETS_FILE))

# Authorize the request and store authorization credentials.
def get_authenticated_service(args):
  flow = flow_from_clientsecrets(CLIENT_SECRETS_FILE, scope=YOUTUBE_READ_WRITE_SSL_SCOPE,message=MISSING_CLIENT_SECRETS_MESSAGE)
  storage = Storage("%s-oauth2.json" % sys.argv[0])
  credentials = storage.get()
  if credentials is None or credentials.invalid:
    credentials = run_flow(flow, storage, args)
  # Trusted testers can download this discovery document from the developers page
  # and it should be in the same directory with the code.
  with open("rest.json", "r",encoding="utf8") as f:
    doc = f.read()
    return build_from_document(doc, http=credentials.authorize(httplib2.Http()))

def get_comments_of_next_page(youtube, video_id,nextPageToken):
    results = youtube.commentThreads().list(
    pageToken=nextPageToken,
    part="snippet",
    videoId=video_id,
    #channelId=channel_id,
    textFormat="plainText"
  ).execute()

    for item in results["items"]:
        comment = item["snippet"]["topLevelComment"]
        author = comment["snippet"]["authorDisplayName"]
        text = comment["snippet"]["textDisplay"]
        publishedAt=comment["snippet"]["publishedAt"]
        print (author.encode('utf8'))
        teststring = text.encode('utf8')
        print ("\t"+str(teststring))
        print ("\t"+str(publishedAt))

    return results["items"]


# Call the API's commentThreads.list method to list the existing comments.
def get_comments(youtube, video_id, channel_id):
  results = youtube.commentThreads().list(
    part="snippet",
    videoId=video_id,
    maxResults=100,
    order='time',
    #channelId=channel_id,
    textFormat="plainText"
  ).execute()


  for item in results["items"]:
    comment = item["snippet"]["topLevelComment"]
    author = comment["snippet"]["authorDisplayName"]
    text = comment["snippet"]["textDisplay"]
    publishedAt=comment["snippet"]["publishedAt"]
    print (author.encode('utf8'))
    teststring = text.encode('utf8')
    print ("\t"+str(teststring))
    print ("\t"+str(publishedAt))

    #call next page 
    nextPageId=str(results["nextPageToken"])
    get_comments_of_next_page(youtube, video_id,nextPageId)
  
    
  return results["items"]

  


  
CH_ID='UC_bdGA9esRWqMSeemKEq5zQ'
argparser.add_argument("--channelid",
   help="Required; ID for channel for which the comment will be inserted.",default=CH_ID)
  # The "videoid" option specifies the YouTube video ID that uniquely
  # identifies the video for which the comment will be inserted.
argparser.add_argument("--videoid",
    help="Required; ID for video for which the comment will be inserted.",default=VIDEO_ID)
  # The "text" option specifies the text that will be used as comment.
argparser.add_argument("--text", help="Required; text that will be used as comment.",default="sankha")
 




args = argparser.parse_args()


youtube=get_authenticated_service(args)


get_comments(youtube, VIDEO_ID, None)

